'''
@file Encoder.py

@brief This file interacts with a physical encoder to measure the position of an gear train ouput shaft. It is intended to be used with a NUCLEO-L476RG board and US digital 1000 cpr encoder.

@param timer Timer using encoder reading capabilities on the Nucleo
        @param period Period that encoder will go to before resetting value. Set at highest 16-bit number
        @param state State for FSM in the .run() function
        @param position The position of the motor since initiaization, total revolutions included
        @param last_position The last position of the motor
        @param interval Interval at which the encoder will read new values. 
        @param start_time the number of microseconds since hardware power on
        @param next_time Next time the encoder will record values.        
'''
import pyb 
import utime

class Encoder:
    '''
    @brief      A FSM used to measure an encoder position
    @details    This class implements a finite state machine to measure
                the output of a specific encoder wired to specific pins on
                the NUCLEO-L476RG board. 
    '''
    
    S0_INIT = 0
    
    S1_Run = 1
    
    def __init__ (self, interval, timer, CH1, CH2):
        '''
        @brief Creates an encoder by initializing timers and pins.
        @details we can’t read one with regular code; we need something faster, 
                 such as interrupts or dedicated hardware. Our STM32 processors have such hardware – 
                 timers which can read pulses from encoders and count distance and direction of motion.
        
        '''
        
        
        ## defining a timer to measure the encoder movement
        self.timer = timer
        
        ## defining a period to be used in the timer
        self.period = 0xFFFF
        
        ## initiallizing the timer, max period
        self.timer.init(prescaler = 0, period = self.period)
        
        ## Fedining channels 1 and 2 of the time, as well as timer mode
        self.timer.channel(1, pin=CH1, mode = pyb.Timer.ENC_AB)
        self.timer.channel(2, pin=CH2, mode = pyb.Timer.ENC_AB)
        
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## the position of the motor since initiaization, total revolutions included
        self.position = 0
        
        ## the last position of the motor
        self.last_position = 0
        
        self.interval = int(interval*1e6)
        
        #the timestamp since the first iteration
        self.start_time = utime.ticks_us() #the number of microseconds since hardware power on
        
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief Finite state machine that records and updates encoder outputs. Used for previous labs.
        @param curr_time Current time in microsecond used to compare if new values should be recorded
        '''
        
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_Run)
                
                # print('encoder init')
            
            elif(self.state == self.S1_Run):
                self.transitionTo(self.S1_Run)
                #print('state 1: waiting 4 cmd')     
                self.update()
            
            else:
                pass
            
            self.next_time =  utime.ticks_add(self.next_time, self.interval)
               
    def update(self):
        '''
        @brief updates the position to the new location of the motor
        '''
        ## need to update this to work in radians?
        self.last_position = self.position
        
        delta = self.get_delta()
        
        self.position = self.position + delta
        
        #print('delta in ticks: ' + str(delta) + '     position: ' + str(self.position) + '     last position: ' + str(self.last_position))
        
        
        
    def get_position(self):
        '''
        @brief Returns the most recent position of the encoder
        '''
        return self.position
    
    
    def set_position(self, newposition):
        '''
        @brief Sets the position of the encoder to a new value defined by user or other functions
        @param newposition Position to set encoder to
        '''
        # print(str(self.position))          ## For debugging
        self.timer.counter(newposition)
        
        
    def get_delta(self):
        '''
        @brief Returns the delta between the last two positions in ticks. Will correct if a period has been passed on the encoder
        @param delta Difference between last two positions in ticks. 
        '''
        delta = self.timer.counter() - self.last_position 
        
        if(delta > ( (self.period/2) )):
            #print('delta is greater than period/2')
            delta = delta - self.period
            
        elif(delta < ( (-1*self.period/2) )):
            #print('delta is less than period/2')
            delta = delta + self.period
            
        else:
            #print('delta passed')
            pass
    
        delta  = delta
        
        return delta
        
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState State to transition to in FSM for .run() function
        '''
        self.state = newState
            
            
        