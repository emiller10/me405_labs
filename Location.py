'''
@file Location.py
@brief This file creates a Location object to interface with the ER-TP080-1 resistive touch panel
@author Ethan Miller
@copyright
'''

import pyb
import utime

class Location():
    '''
    
    '''
    
    def __init__(self, y_m, x_m, y_p, x_p, length, width):
        '''
        @brief Initialization of Location class that takes input variables and converts them to self variables that can be used in other functions of class.
        @param y_m Y- Pin connected to breakout board on touchpad
        @param x_m X- Pin connected to breakout board on touchpad
        @param y_p Y+ Pin connected to breakout board on touchpad
        @param x_p X+ Pin connected to breakout board on touchpad
        @param length Length of long side of touch panel
        @param width Width of short side of touch panel
        @param adc ADC object used to read pin voltages
        '''
        self.y_m = y_m
        self.x_m = x_m
        self.y_p = y_p
        self.x_p = x_p
        
        self.length = length
        self.width = width
        
    def xScan(self):
        '''
        @brief This method scans the x component of touchpad
        @details This method scans the x component by configuring self.xp to a push pull output set high and self.xm to a push pull output set low. The voltage is then measures by floating yp and measuring the voltage at ym.
        @return Double of x position in inches from origin
        '''
        pyb.Pin(self.x_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.y_p, mode = pyb.Pin.IN)
        pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.x_p.high()
        self.x_m.low()
        adc = pyb.ADC(self.y_m)
        return self.length*((adc.read()/4095)-0.5) 
            
    def yScan(self):
        '''
        @brief This method scans the y component of touchpad
        @details This method scans the y component by configuring self.yp to a push pull output set high and self.ym to a push pull output set low. The voltage is then measures by floating xp and measuring the voltage at xm.
        @return Double of y position in inches from origin
        '''
        pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.y_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        pyb.Pin(self.x_m, mode = pyb.Pin.IN)
        self.y_p.high()
        self.y_m.low()
        adc = pyb.ADC(self.x_m)
        return self.length*((adc.read()/4095)-0.5)
        
    def zScan(self):
        '''
        @brief This method determines if there is contact on the touchpad
        @details This method scans the z component by configuring self.yp to a push pull output set high and self.xm to a push pull output set low. The voltage is then measuring either the ym or xp pins. The ym pin will be high if there is no contact on the board, but lower than high if there is contact.
        @return Boolean: False if ym pin is high, else True
        '''
        pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.y_p.high()
        self.x_m.low()
        adc = pyb.ADC(self.y_m)
        z = adc.read()
        if z < 4000:
            return True
        else:
            return False
        
    def xyzScan(self):
        '''
        @brief Function that will call all of the position functions
        @return List of x and y positions and z boolean for if contact is made.
        '''
        return (self.xScan(),self.yScan(),self.zScan())
        
if __name__ == '__main__':
    length = 0
    width = 0

    y_m = pyb.Pin.cpu.A0
    x_m = pyb.Pin.cpu.A1
    y_p = pyb.Pin.cpu.A6
    x_p = pyb.Pin.cpu.A7


    while True:
        x = Location(y_m, x_m, y_p, x_p,length = 177,width = 99)
        start = utime.ticks_us()
        x.xyzScan()
        end = utime.ticks_us()
        print(utime.ticks_diff(end,start))
