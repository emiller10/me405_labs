# -*- coding: utf-8 -*-
"""
@file UI_lab3.py
@brief  
@details 
@param 
@author Ethan Miller
@copyright
@date February 23, 2021

"""

import serial
import keyboard
import pyb
import matplotlib.pyplot as plt
import csv

ser = serial.Serial(port='COM3',baudrate=115200)
    
# initialize variables
last_key = None

# set up keyboard interrupts
keyboard.on_release_key('g', callback = keyboardCallback)
myval = None


def keyboardCallback(key):
    '''
    @brief Callback function is called anytime a key is pressed and released on the users keyboard
    @details  On release of the key, this function will be called to store the last key pressed
    @param  last_key Global variable that stores the last key pressed
    @return  NONE

    '''
    global last_key
    last_key = key.name
    print('hello interrupt')
    print(str(last_key))

while True:
    try:
        if last_key == 'g':
            ser.write(last_key.encode('ascii'))
            pyb.delay(4000)
            if ser.readall():
                data = ser.readall().decode('ascii')
                data = data.strip().replace('''array('H', ''','')
                data = data.strip().replace('''array('f',''','')
                data = data.strip().replace('(','')
                data = data.strip().replace(')','')
                
                volts = [float(s) for s in rawData[0].split(',')]
                volts = [3.3*val/4095 for val in volts]
                timeData = [float(s)*1000 for s in rawData[1].split(',')]
                
                ## plotting data
                plt.plot(timeData, volts)
                ax=plt.axes()
                ax.grid()
                
                plt.title('Button Voltage vs Reference Voltage')
                plt.ylabel('Voltage, (V)')
                plt.xlabel('Time, (ms)')
                plt.legend()
                plt.show
                
                ## saving data into CSV, could not figure out how to put it in columns
                with open('data.csv', 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(volts)
                writer.writerow(timeData)
          
    except KeyboardInterrupt:
        ser.close()
        break












