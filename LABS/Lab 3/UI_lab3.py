# -*- coding: utf-8 -*-
"""
@file UI_lab3.py
@brief  This is the UI front that communicates through UART to the Nucleo to take data. It also stores data taken into a .csv file
@details This UI front communicates over a UART to take data on the voltages output by pressing the blue User button on the Nucleo.
         This is intended for the NUCLEO-L476RG board which must have pin PC13 wired to pin PA0.
@param last_key Variable that stores the last key pressed. Set by keyboardCallback
@param ser Serial port used to communicate with Nucleo
@param timeData Time data that corresponds with the voltage data
@param data Voltage data from the Nucleo for the step response of the blue User button 
@author Ethan Miller
@copyright
@date February 23, 2021
"""

import serial
import keyboard
import time
import matplotlib.pyplot as plt
import csv


def keyboardCallback(key):
    '''
    @brief Callback function is called anytime a key is pressed and released on the users keyboard
    @details  On release of the key, this function will be called to store the last key pressed
    @param  last_key Global variable that stores the last key pressed
    @return  NONE

    '''
    global last_key
    last_key = key.name
    
    
ser = serial.Serial(port='COM3',baudrate=115200)

    
# initialize variables
last_key = None

# set up keyboard interrupts
keyboard.on_release_key('g', callback = keyboardCallback)
data = ""  

while True:
    try:
        
        print('When you are ready to start, enter \'G\'.\n')  
        cmd = input('')
        
        if ((cmd == 'g') or (cmd == 'G')):
            ser.write('G'.encode('ascii'))
                
            if ser.readall():
                data = ser.readall().decode('ascii')
                data = data.strip().replace('''array('H', ''','')
                data = data.strip().replace('''array('f',''','')
                data = data.strip().replace('(','')
                data = data.strip().replace(')','')
                
                data = [float(s) for s in data[0].split(',')]
                data = [3.3*val/4095 for val in data]
                timeData = [float(s)*1000 for s in rawData[1].split(',')]
                
                ## plotting data
                plt.plot(timeData, data)
                ax=plt.axes()
                ax.grid()
                
                plt.title('Button Voltage vs Reference Voltage')
                plt.ylabel('Voltage, (V)')
                plt.xlabel('Time, (ms)')
                plt.legend()
                plt.savefig('ADC_plot.png')
                
                ## saving data into CSV, could not figure out how to put it in columns
                with open('ADC_data.csv','w') as file:
                    # Iterate through rows of data
                    for index in range(len(timeData)):
                        file.write('{:f},{:f}\n'.format(timeData[index],data[index])) # Write a row of a data
                        
                print('The ADC step response data has been exported as: "ADC_data.csv"')
                break
                
    except KeyboardInterrupt:
        ser.close()
        break
    











