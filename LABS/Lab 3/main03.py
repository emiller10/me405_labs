
        # What am I trying to do?
        # -Create a UI interface that communicates with Nucleo and sends an signal in the form of a 'g' when we want to start code
        # -Class that for the UI interface that can be used in later labs
        # -Connect blue button to the ADC 
import keyboard
import serial


class UI_interface:
    '''
    
    '''
    def __init__(self):  #scipt that will run when object of this class is created
        '''
        
        '''
        #
        
        #--------VARIABLES-----------
        
        # User interface buttons
        self.g_pressed = False
        self.last_key = ''
        self.state = 0
        keyboard.on_release(self.keyboardCallback)

#------------------------------------------------------------------------------------------               
    
    def g_Pressed(self):
        '''
        @brief
        @details  
        @param  
        
        @return 
        
        '''
        self.g_pressed = not self.g_pressed
        
#------------------------------------------------------------------------------------------                   
        
    def keyboardCallback(self,key):
            '''
            @brief THIS FUNCTION IS CALLED ANY TIME A KEY IS PRESSED AND RELEASED ON THE KEYBOARD
            @details  ON RELEASE OF THE KEY, THIS FUNCTION WILL BE CALLED TO STORE THE LAST KEY PRESSED
            @param  last_key GLOBAL VARIABLE THAT STORES THE LAST KEY PRESSED
            
            @return  NONE
        
            '''
            self.last_key = key.name
            print(self.last_key)   
            self.g_pressed = True
            
            
#------------------------------------------------------------------------------------------            

    def init_Message(self):
        '''
        @brief
        @details  
        @param  
        
        @return 
        
        '''
        print("Hey! Want some semi-usless data?! \n Have I got the thing for you! Just press the 'G' button and get started!")
        
#------------------------------------------------------------------------------------------                   
        
        
    def run(self):
        '''
        @brief This is the finite state machine that will run with the UI interface is created and ran.
        @details   
        @param  
        
        @return 
        
        
        State 0 - display initialization message 
        State 1 - wait until a 'g' is pressed
        State 2 - Send the 'g' to the Nucleo over UART
        State 3 - wait for data and receive it
        State 4 - Take the data and transfer it into a .csv file
        State 5 - Plot the data and output the graph to a file. 
        
        
        '''
        while True:
            if self.state == 0:
                self.init_Message()
                self.state = 1
           
            if self.state == 1:
                if self.g_pressed:
                    print('You pressed g! Time for the fun! Wait for the data to come in')
                    self.state = 2
           
            if self.state == 2: 
                ser.write('g'.encode('ascii'))
                print('\'g\' is sent')
                self.state = 3
                
            if self.state == 3:
                
            if self.state == 4:
                
            if self.state == 5:
                
#------------------------------------------------------------------------------------------           
                
if __name__ == '__main__':    
    ## A UI_Front task object
    task = UI_interface()

    while True:
        task.run()
        pass




















