# -*- coding: utf-8 -*-
"""
@file main_lab3.py
@brief Main script that runs on the Nucleo to communicate with PC to record the step response of pushing the User button.
@param freq Frequency at which the data will be taken from the User button
@param data_length Number of data points to be stored
@param pin_A0 Pin that connects to ADC
@param adc ADC object that can be read
@param ADC_buf Buffer that will store voltage from pin_A0
@param time_buf Buffer that will store time when voltage is taken for each data point
@param myuart UART object that allows connection to PC
@author Ethan Miller
@copyright
@date February 23, 2021

"""
import utime   #import utime to use .sleep function for turning on and off LED
import pyb     #import pyb to interact with Nucleo board pins 
import array   #import array to you .array function on output arrays
from pyb import UART

myuart = UART(2)

freq = 100000
data_length = 500

# setting up pin_A0 to be an ADC
pin_A0 = pyb.Pin(pyb.Pin.board.PA0, pyb.Pin.IN)
adc = pyb.ADC(pin_A0)

time_buf = array.array('f', (0 for index in range (data_length)))
ADC_buf = array.array ('H', (0 for index in range (data_length)))

n = 0
for n in range(0,data_length):
    time_buf[n] = n/freq

while True:
    ## check if 'g' has been pressed by user
    if myuart.readchar() == 103:
        
        while True:
            ## checking for button press
            if adc.read() == 0: # button is pressed
                
                ## need if statenment for button release or else its all 0s
                if adc.read() > 0:
                    ## reading adc data
                    adc.read_timed(buffy, freq)  
                    break
    
        ## sending all data to Front end
        myuart.write(str(ADC_buf) + '/' + str(time_buf))














