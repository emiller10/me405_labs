# -*- coding: utf-8 -*-
"""
@file lab3_main.py

@brief  

@details 

----------------------------------------------
Parameters:

@param 

----------------------------------------------


@author Ethan Miller
@copyright
@date February 23, 2021

"""

import utime   #import utime to use .sleep function for turning on and off LED
import pyb     #import pyb to interact with Nucleo board pins 
import array   #import array to you .array function on output arrays


pin_A0 = pyb.Pin(pyb.Pin.board.PA0, pyb.Pin.IN)
timer = pyb.Timer(2, period = 0x7FFFFFFF, prescaler = 79)
adc = pyb.ADC(pin_A0)
ADC_buf = array.array('H', (4095 for index in range(500))) #creating a 2-bit array for rolling storage for ADC
time_buf = array.array('H', (0 for index in range(500))) #creating a 2-bit array for rolling storage for Timer

history = 10
n = 0
g_pressed = True
goodstep = False
timer.counter(0)
last_time = timer.counter()

while True:
    
    # Get the values for the ADC and timer, this should be done everytime and roll
    ADC_buf[n] =  adc.read()
    time_buf[n] = timer.counter()
    
    if g_pressed:
        
        if ADC_buf[n] < 10 and goodstep == False:
            goodstep = True
            history = 50
        elif goodstep:
            if history == 500:
                print("Bufferlist: ")
                print(ADC_buf)
                # print(time_buf)
                goodstep = False
                history = 0
                g_pressed = False
                break
            else:
                history += 1
    n += 1
    if n == 200:
        n = 0
        






        # if ADC_buf[n] < 1000 and history < 10:
            # if goodstep:
                # history += 1
                    # if history == 200:
                        # print("Bufferlist: ")
                        # print(ADC_buf)
                        # print(time_buf)
                        # goodstep = False
                        # history = 0
                        # g_pressed = False
                        # pass
            # history += 1
            # if history == 10:
                # goodstep = True
        # else:
            # history = 0

    
    
    
    

























