clear all
close all

format compact
format short

syms x
syms x_dot
syms theta_y
syms theta_dot_y
syms T_x

g = 9.806; % Gravity (m / s^2)
b = 10e-3; % Viscous friction at U-Joint (N m s /rad)

l_p = 110e-3;  % Length of platform (m)
l_r = 50e-3;   % Length of motor rod (m)
r_b = 10.5e-3; % Radius of ball (m)
r_g = 42e-3;   % Distance from platform pivot to center of G (m)
r_p = 32.5e-3; % Distance to center of platform (m)
r_c = 50e-3;   % Distance to surface of platform (m)
r_m = 60e-3;   % Length of motor arm (m)

m_p = 400e-3;  % Mass of platform (kg)
m_b = 30e-3;   % Mass of ball (kg)

I_b = (2/5)*m_b*r_b^2   % Inertia of ball (kg m^2)
I_p = 1.88e-3           % Inertia of platform (kg m^2)

                                
M_11 = m_b*(r_b^2) + m_b*r_c*r_c + I_b; % (kg m^2)
M_12 = -( I_b*r_b + I_p*r_b + m_b*r_b^3 + m_b*r_b*r_c^2 + 2*m_b*r_b^2*r_c + m_p*r_b*r_g^2 + m_b*r_b*x^2)/r_b; % (kg m^2)
M_21 = -(m_b*r_b^2 + I_b)/r_b; % (kg m^2)
M_22 = -(m_b*r_b^3 + m_b*r_c*r_b^2+I_b*r_b)/r_b; % (kg m^2)
M = [M_11 M_12;
     M_21 M_22]; % (kg m^2)

f_11 = b*theta_dot_y - g*m_b*(sin(theta_y)*(r_b+r_c)+x*cos(theta_y)) + (T_x*l_p)/r_m + 2*m_b*theta_dot_y*x*x_dot - g*m_p*r_g*sin(theta_y);
f_21 = -m_b*r_b*x*theta_dot_y^2 - g*m_b*r_b*sin(theta_y);
f = [f_11;
     f_21];


q = inv(M)*f


A_11 = diff(q(1), x_dot);
A_12 = diff(q(1), theta_dot_y);
A_13 = diff(q(1), x);
A_14 = diff(q(1), theta_y);
A_21 = diff(q(2), x_dot);
A_22 = diff(q(2), theta_dot_y);
A_23 = diff(q(2), x);
A_24 = diff(q(2), theta_y);

A = [A_11 A_12 A_13 A_14;
     A_21 A_22 A_23 A_24
     1    0    0    0  
     0    1    0    0 ];

B_11 = diff(q(1),T_x);
B_21 = diff(q(2),T_x);

B = [B_11;
     B_21;
     0;
     0];

A = subs(A,x,0);
A = subs(A,x_dot,0);
A = subs(A,theta_y,0);
A = subs(A,theta_dot_y,0);
B = subs(B,x,0)

A = vpa(A,4)
B = vpa(B,4)
The A and B matrices are the open-loop system matrices. We can use them to determine the close loop gains for the controller we are creating using , that is:

C = eye(4);
D = [0;0;0;0]

% creating statespace
sys = ss(A,B,C,D, 'statename', {'x_d [m/s]' 'thetay_d [rad/s]' 'x [m]' 'theta_y [rad]'}, ...
    'inputname', 'T_x [Nm]', 'outputname', {'x_d [m/s]' 'thetay_d [rad/s]' 'x [m]' 'theta_y [rad]'}) 

syms K_1 K_2 K_3 K_4
k = [K_1 K_2 K_3 K_4];
B_k = mtimes(B,k);
A_cl = A - (B_k);
syms s
P_cl = det(s*I - A_cl);
P_cl_coeff = vpa(coeffs(P_cl,s)',5)

Now we can find the characteristic polynimial of the desired fourth-order system by using a damping ratio and natural frequency of our choosing. In order to find the  and  for the system, we looked at the effects that they would have on a generic second-order system with a step response shown in Figure 1A.1. Knowing that extensive oscillation at the beginning could cause problems with keeping the ball on the platform, we decided to choose a damping ration close to 1, in this case 0.95. 

We can now use the time domain characteristics of the system to select and appropiate natural frequency from Figure 1A.2. We will be looking at the settling time of the device, , shown in Equation 1A.5. 


This equation will allow us to select a  by choosing a settling time in seconds. 
zeta = 0.95;
t_settle = .75;  % second
w_n = 3/(t_settle*zeta)

des = s^2 + 2*zeta*w_n*s + w_n^2;
roots_des = solve(des);
roots_des(end+1) = 10*real(roots_des(1));
roots_des(end+1) = 11*real(roots_des(2));

P_des = expand(prod(s-roots_des));
P_des_coeff = vpa(coeffs(P_des,s)',5)


eq1 = P_cl_coeff(1) == P_des_coeff(1);
eq2 = P_cl_coeff(2) == P_des_coeff(2);
eq3 = P_cl_coeff(3) == P_des_coeff(3);
eq4 = P_cl_coeff(4) == P_des_coeff(4);
eq5 = P_cl_coeff(5) == P_des_coeff(5);

K = solve([eq1 eq2 eq3 eq4 eq5], [K_1 K_2 K_3 K_4]);
K = vpa([K.K_1 K.K_2 K.K_3 K.K_4],5)
