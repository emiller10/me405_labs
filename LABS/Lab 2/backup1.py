# -*- coding: utf-8 -*-
"""
@file main.py



"""

import utime
import pyb
import random

timer = pyb.Timer(5)  #create a Timer object (timer 2) to count upwards from 0
timer.init(prescaler = 80, period = 0x7FFFFFFF, mode = timer.UP)
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
off_time = 0
n = 0
response_list = []
global looking_for_button

def button_interrupt(which_timer):
    global off_time
    if looking_for_button:
        off_time = timer.counter()
    
   
extint = pyb.ExtInt(pyb.Pin.board.PC13, 
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    button_interrupt)
    
# main code
utime.sleep(1)

while True:
    try:
        utime.sleep(random.randint(2,3))
        blinko.high()
        timer.counter(0)
        looking_for_button = True
        utime.sleep(1)
        blinko.low()
        
        if off_time != 0:
            response_list.append(off_time/(10**6))
            n += 1
            off_time = 0
        else:
            pass
       
        looking_for_button = False
        
    except KeyboardInterrupt:
        if len(response_list) != 0:
            react_time = sum(response_list)/len(response_list)
            print("\nYour average reaction time was {:.3f} seconds \n".format(react_time))
        else:
            print("\nNo valid attempts were recorded \n")
        break

    
    
    
    