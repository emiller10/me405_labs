"""
@file lab1_main.py
@brief Virtual vending machine that computes monetary transactions for soft drinks
@details Virtual vending machine that uses python script to take user input which represent money (from pennies to $20 bills) and drink choices, and gives drinks accordingly
@author Ethan Miller
@date February 16, 2021
@copyright
""" 
import keyboard
import time

# -----------------------------------------------------------       
def getChange(price, payment):
    '''
    @brief COMPUTES CHANGE FROM MONETARY VALUE
    @details  GIVEN A SET OF COINS, BILLS AND A PRICE, COMPUTES CHANGE IN FEWEST
              BILLS/COINS
    @param  price THE PRICE AS INTEGER # OF CENTS
    @param  payment A TUPLE DESCRIBING THE QUANTITY OF EACH DENOMINATION IN ASCENDING ORDER
    
    @return  IF FUNDS ARE SUFFICIENT, RETURNS A TUPLE OF CHANGE, IF INSUFFICIENT, RETURNS NONE

    '''
    
    # payment AND price ARE BROUGHT INTO THE FUNCTION AS TUPLES WITH INDEX:  
    #    0: PENNIES , 1: NICKELS , 2: DIMES , 3: QUARTERS , 4: 1 DOLLAR BILLS
    #    5: 5 DOLLAR BILLS , 6: 10 DOLLAR BILLS , 7: 20 DOLLAR BILLS
    
    # WE WILL CONVERT THIS ALL INTO CENTS
    index_values = (1,5,10,25,100,500,1000,2000) # PRICE IN CENTS FOR INDEX VALUES ABOVE
    payment_cents = 0
    
    for x in range(0,len(index_values)):     # FOR LOOP CHANGING TUPLE INTO SINGLE VALUES IN CENTS
        payment_cents += payment[x]*index_values[x]
    pass

    change_cents = payment_cents-price  # CALCULATING CHANGE
    change_list = [0]*len(index_values)
    
    if change_cents>0:                        #TESTING IF FUNDS ARE SUFFICIENT
        # CONVERT CHANGE BACK INTO INDEXED LIST
        for y in range(0,len(index_values)):
            while change_cents-index_values[len(index_values)-y-1]>=0:
                change_list[len(index_values)-y-1]+=1
                change_cents -= index_values[len(index_values)-y-1]
            pass
        change = tuple(change_list)
        return change
    else:
        return None
    
# -----------------------------------------------------------       
def keyboardCallback(key):
    '''
    @brief THIS FUNCTION IS CALLED ANY TIME A KEY IS PRESSED AND RELEASED ON THE KEYBOARD
    @details  ON RELEASE OF THE KEY, THIS FUNCTION WILL BE CALLED TO STORE THE LAST KEY PRESSED
    @param  last_key GLOBAL VARIABLE THAT STORES THE LAST KEY PRESSED
    
    @return  NONE

    '''
    global last_key
    last_key = key.name

# -----------------------------------------------------------       
def printWelcome():
    """
    @brief Prints a Vendotron^TM^ welcome message with beverage prices
    """
    print("HI! Welcome to the Vendotron, your personal hydration assistant." + 
          "Simply enter your form of false currency that has no value to my tiny computer brain by pressing 0 for pennies, 1 for nickles, 2 for quarter, and so on." +
          "After entering the correct amount, you can order a non-copywritten beverage of your choosing! Press C for Cuke, P for Popsi, S for Spryte, or D for Dr. Pupper."+
          "At any time, feel free to break my heart and press the E button to eject your change and leave me forever. Thank you for choosing Vendotron!")
    pass


# -----------------------------------------------------------  
state = 0
balance_p = 0 #balance_p in pennies
balance = [0,0,0,0,0,0,0,0] #balance as a list
price = 0   #price in pennies
fund_names = ("Pennies", "Nickels", "Dimes", "Quarters", "Dollar Bills", "5 Dollar Bills", "10 Dollar Bills", "20 Dollar Bills")

last_input_time = 0
last_key = ''
message = ''
first_line = 0 #flag that is used to determine if this is the first line when outputing the change given after EJECT

pCuke = 100    #price for Cuke (100 cents)
pPopsi = 120   #price for Popsi (120 cents)
pSpryte = 85   #price for Spryte (85 cents)
pPupper = 110  #price for Dr.Pupper (110 cents)

keyboard.on_release_key(("c","p","s","d","e"),callback = keyboardCallback) # calls the keyboard_callback command when certain key is pressed
keyboard.on_release_key(("0","1","2","3","4","5","6","7"),callback = keyboardCallback)



# -----------------------------------------------------------       

while True:
        """Implement FSM using a while loop and an if statement that will runt 
        eternally until user presses CTRL-C
        """
        if state == 0:
            """perform state 0 operations
            """
            printWelcome()
            state = 1
     # -----------------------------------------------------------            
        elif state == 1:                         # STANDBY STATE  
            """
            STANDBY state - This state is wating for an input from the user.
            """
            if last_key == '':
                if(time.perf_counter()-last_input_time > 20):
                    message = "Hey! Try Cuke today!"
                    state = 2
                    last_input_time = time.perf_counter()
                else:
                    pass    #nothing has been pressed, so continue 
            elif last_key == "e":
                state = 5
                last_input_time = time.perf_counter()
            elif last_key.isnumeric():
                state = 3
                last_input_time = time.perf_counter()
            elif last_key.isalpha():
                state = 4
                last_input_time = time.perf_counter()
            else:
                pass
      # -----------------------------------------------------------  
        elif state == 2:                         # DISPLAY STATE
            if not message == "":
                print(message)
                message = ''
            state = 1
      # -----------------------------------------------------------   
        elif state == 3:                         # WHAT COIN? STATE
            if last_key == "0":  # Penny
                message = "Ugh, thanks for the penny..."
                balance[0] += 1
                balance_p += 1
            
            elif last_key == "1":  # Nickel
                message = "You could take your Nickelback..."
                balance[1] += 1
                balance_p += 5
            
            elif last_key == "2":  # Dime
                message = "I'm ten cents richer..."
                balance[2] += 1
                balance_p += 10
            
            elif last_key == "3":  # Quarter
                message = "Two of these could make a rapper..."
                balance[3] += 1
                balance_p += 25
           
            elif last_key == "4":  # Dollar Bill
                message = "I need a Washington..."
                balance[4] += 1
                balance_p += 100
           
            elif last_key == "5":  # 5 Dollar bill
                message = "Lincoln me up..."
                balance[5] += 1
                balance_p += 500
            
            elif last_key == "6":  # 10 Dollar bill
                message = "Isn't he a musical?..."
                balance[6] += 1
                balance_p += 1000
           
            elif last_key == "7":  # 20 Dollar Bill
                message = "Oh baby, Jackson in the house..."
                balance[7] += 1
                print(balance)
                balance_p += 2000
            
            message += "balance = $" + "{:.2f}".format(balance_p/100)
            last_key = ""
            state = 2
      # -----------------------------------------------------------       
        elif state == 4:                         # DRINK SELECTED STATE
            if last_key == "c":
                price = pCuke
                message = "Ah! Cuke is always an excellent choice!"
            elif last_key == "p":
                price = pPopsi
                message = "Popsi...Is it better than Cuke, though?"
            elif last_key == "s":
                price = pSpryte
                message = "Spryte! But this isn't McDonalds..."
            elif last_key == "d":
                price = pPupper
                message = "Dr. Popper...I guess..."
            else:
                state = 1
                pass
            
            if balance_p - price < 0:
                message = "Insufficient Funds! Find some more change in Momma's purse!"
            else:
                payment = (balance[0],balance[1],balance[2],balance[3],balance[4],balance[5],balance[6],balance[7])
                new_balance = getChange(price,balance)
                balance = [new_balance[0],new_balance[1],new_balance[2],new_balance[3],new_balance[4],new_balance[5],new_balance[6],new_balance[7]]
                balance_p -= price
                message += " Thanks for shopping with Vendotron! Press E to eject or buy another drink! balance = $"+"{:.2f}".format(balance_p/100)
                            
            last_key = ""
            state = 2
       # -----------------------------------------------------------       
        elif state == 5:                         # EJECT STATE
            message = "Aw man, I guess you are saying goodbye, thanks for shopping. Returning your $"+"{:.2f}".format(balance_p/100)
            message += "\n Returned: " # This will look at the actual currency that is going to be returned
            for x in range(0,len(balance)): #For loop for however many inputs balance takes (pennies to twenties)
                if balance[x] > 0:          # If the balance in each component of list is 0, don't return that currency type
                    if not first_line==0:         # flag to determine if this is the first thing being returned
                        message += ", "+str(balance[x])+" "+fund_names[x]  #return that many of the currency
                    else:
                        first_line = 1            # set flag once first message is returned
                        message += str(balance[x])+" "+fund_names[x]
                else:
                    pass
            #reset variables after change is given
            first_line = 0   
            balance_p = 0
            payment = [0,0,0,0,0,0,0,0]
            last_key = ""
            state = 2
      
