'''

'''

import pyb
import utime

class Encoder:
    '''
    
    '''
    
 
#---------------------------------------------------------------------------------------------------------  
 
    def __init__(self, interval, timer, CH1, CH2):
        '''
        
        '''
        #redefine input parameters so they can be used in other functions of the class
        self.timer = timer
        self.CH1 = CH1  
        self.CH2 = CH2
        self.interval = interval
        
        # set up given timer to be ticked when CH1 or CH2 pin changes voltage
        # This is done using the .channel function and setting up the 1st channel to the CH1 pin in ENC_AB mode from pyb.Timer library
        self.timer.channel(1, pin = self.CH1, mode = pyb.Timer.ENC_AB)
        self.timer.channel(2, pin = self.CH2, mode = pyb.Timer.ENC_AB)
        
        # Setting up other variables
        self.period = 0xFFFF
        self.position = 0
        self.last_position = 0
        self.start_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.start_time, self.interval)
  
#---------------------------------------------------------------------------------------------------------  
  
    def update(self):
        '''
        
        '''
 
#---------------------------------------------------------------------------------------------------------  
 
    def get_positions(self):
        '''
        
        '''
    
#---------------------------------------------------------------------------------------------------------  
    
    def set_position(self):
        '''
        
        '''
        
#---------------------------------------------------------------------------------------------------------        
        
    def get_delta(self):
        '''
        
        ''' 