'''
@file
'''

import pyb



class MotorDriver:
    '''
    
    '''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, CH1, CH2, timer):
        '''
        @description
        
        @param nSLEEP_pin Pin that can be set to high or low voltage in order to enable and disable motors
        @param IN1_pin First input to half bridge 1
        @param IN2_pin 
        
        '''
        
        # save variables to object:
        self.nSLEEP_pin = nSLEEP_pin
        # self.nFAULT_pin = nFAULT_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        
        # other variables:
        self.duty = 0
        self.status = 'OFF'
        self.fault = False
        
        # setting up the motor:
        self.motor_CH1 = timer.channel(CH1, mode = pyb.Timer.PWM, pin = self.IN1_pin)
        self.motor_CH2 = timer.channel(CH2, mode = pyb.Timer.PWM, pin = self.IN2_pin)
        
        self.motor_CH1.pulse_width_percent(0)
        self.motor_CH2.pulse_width_percent(0)
        
        # Fault detection
        # self.extint = pyb.ExtInt(pyb.Pin(pyb.Pin.cpu.B2, mode = pyb.Pin.IN),              #Setting pin nFAULT_pin to activate an EXTERNAL INTERRUPT
                            # pyb.ExtInt.IRQ_FALLING,     #Interrupt occurs on falling edge of signal
                            # pyb.Pin.PULL_NONE,            #Pull up resister is being used for this pin
                            # self.fault_callback)             #Define what interrupt service routine to go to when activated (fault_callback)
        
    def enable(self):
        '''
        Enables Motor: set the nSLEEP pin to HIGH
        '''
        # self.extint.disable()
        self.nSLEEP_pin.high()
        pyb.delay(100)
        # self.extint.enable()
        self.status = 'ON'
        self.motor_CH1.pulse_width_percent(0)
        self.motor_CH2.pulse_width_percent(0)
    
    def disable(self):
        '''
        Disable Motor: set the nSLEEP pin to LOW
        '''
        self.nSLEEP_pin.low()
        self.status = 'OFF'
        self.set_duty(0)
        
    def motor_status(self):
        print(self.status)
    
    def set_duty(self,duty):
        '''
        Set duty cycle to given value
        '''
        self.duty = duty
        if self.duty > 0:
            self.motor_CH1.pulse_width_percent(self.duty)
            self.motor_CH2.pulse_width_percent(0)
        elif self.duty < 0:
            self.motor_CH1.pulse_width_percent(0)
            self.motor_CH2.pulse_width_percent(-1*self.duty)
        else:
            self.motor_CH1.pulse_width_percent(0)
            self.motor_CH2.pulse_width_percent(0)
    
    # def fault_callback(self, pin):
        # print('nFAULT')
        # self.extint.disable()
        # self.disable()
        # self.fault = True
        
        
    # def fault_input_pass(self):
        # password = input("Please put password in to bypass nFAULT:\n")
        # while password != "rawr":
            # password = input("Please insert valid password:\n")
        
        # if password == "rawr":
            # print("Thanks! You're good!\n")
            # self.extint.enable()
            # self.enable()
            # self.fault = False
           
        
            
        
    
    