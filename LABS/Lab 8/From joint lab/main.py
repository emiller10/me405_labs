from pyb import Timer, Pin, delay
from MotorDriver import MotorDriver
from fault import nFAULT

timer_3 = Timer(3, freq=20000)

IN1 = pyb.Pin.cpu.B4
IN2 = pyb.Pin.cpu.B5
IN3 = pyb.Pin.cpu.B0
IN4 = pyb.Pin.cpu.B1

nSLEEP = pyb.Pin.cpu.A15
nFAULT_pin = pyb.Pin.cpu.B2


CH1 = 1
CH2 = 2
CH3 = 3
CH4 = 4


M1 = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
M2 = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)

M1.enable()
M2.enable()

fault = nFAULT(nSLEEP)


# extint = pyb.ExtInt(pyb.Pin(pyb.Pin.cpu.B2, mode = pyb.Pin.IN),              #Setting pin nFAULT_pin to activate an EXTERNAL INTERRUPT
                            # pyb.ExtInt.IRQ_FALLING,     #Interrupt occurs on falling edge of signal
                            # pyb.Pin.PULL_NONE,            #Pull up resister is being used for this pin
                            # fault_callback)             #Define what interrupt service routine to go to when activated (fault_callback)

# def fault_callback(pin):
        # print('nFAULT')
        # extint.disable()
        # fault = True


while True:

    try:
        
        M1.set_duty(80)
        delay(500)
        M1.set_duty(0)
        delay(500)
        M1.set_duty(-80)
        delay(500)
        M1.set_duty(0)
        delay(500)
        
        M2.set_duty(50)
        delay(500)
        M2.set_duty(0)
        delay(500)
        M2.set_duty(-50)
        delay(500)
        M2.set_duty(0)
        delay(500)
        
    except:
        M1.disable()
        M2.disable()
        print('See ya')
        break
    
   
