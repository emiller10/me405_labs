import pyb
import utime
from Location import Location

length = 177 # millimeters
width = 99  # millimeters

y_m = pyb.Pin.cpu.A0
x_m = pyb.Pin.cpu.A1
y_p = pyb.Pin.cpu.A6
x_p = pyb.Pin.cpu.A7

while True:
    x = Location(y_m, x_m, y_p, x_p, length, width)
    # start = utime.ticks_us()
    print(x.xyzScan())
    # end = utime.ticks_us()
    # print(utime.ticks_diff(end,start))