# -*- coding: utf-8 -*-
"""
@file main.py


@author: Cole
"""

import pyb
import utime

class platformController:
    '''
    an FS
    '''
    
    ## Init state
    S0_INIT = 0
    ## State 1: updating positions and speed
    S1_Values = 1
    ## State 2: motor control
    S2_Motors = 2
    
    def __init__ (self, period, mot_x, mot_y, enc_x, enc_y, touchScreen, K):
        '''
        creates a controller to balance the platform.
        @param k is a list representing gain values
        '''
        self.period = period
        
        ## defining motors and controllers
        self.mot_x = mot_x
        self.mot_y = mot_y
        self.enc_x = enc_x
        self.enc_y = enc_y
        self.touchScreen = touchScreen
        
        ## defining gain values
        self.K1 = K[0]
        self.K2 = K[1]
        self.K3 = K[2]
        self.K4 = K[3]
        
        ## init positions and speed for motor x
        self.x = 0
        self.xd = 0
        self.theta_y = 0
        self.thetad_y = 0
        
        ## init positions and speed for motor y
        self.y = 0
        self.yd = 0
        self.theta_x = 0
        self.thetad_x = 0
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        #the timestamp since the first iteration
        self.start_time = utime.ticks_us() #the number of microseconds since hardware power on
        
        self.next_time = utime.ticks_add(self.start_time, int(self.period*1e6))       
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        
        if(utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
            if(self.state == self.S0_INIT):
                ##
                self.mot_x.enable()
                self.mot_y.enable()
                self.enc_x.set_position(0)
                self.enc_y.set_position(0)
                self.transitionTo(self.S1_Values)
            
            elif(self.state == self.S1_Values):
                
                new_xyz = self.touchScreen.xyzScan()
                self.enc_y.update()
                self.enc_x.update()
                
                ## updating positions for x direction
                self.xd = (new_xyz[0] - self.x)/self.period
                self.x = new_xyz[0]
                
                
                #############################################################
                
                ## angle and angular velocity
                self.theta_y = self.enc_x.get_position()*0.001571*0.06/0.110
                self.thetad_y = self.enc_x.get_delta()*0.001571*0.06/(0.110*self.period)
                
                #############################################################
                
                ## updating positions for y direction
                self.yd = (new_xyz[1] - self.y)/self.period
                self.y = new_xyz[1]
                
                
                #############################################################
                
                self.theta_x = self.enc_y.get_position()*0.001571*0.06/0.110
                self.thetad_x = self.enc_y.get_delta()*0.001571*0.06/(0.110*self.period)
            
                #############################################################
                
                
                self.transitionTo(self.S2_Motors)
            
            elif(self.state == self.S2_Motors):
                
                
                #K= R/(Vdc*kt)
                
                Tx = -self.K1*self.xd - self.K2*self.thetad_y - self.K3*self.x - self.K4*self.theta_y
                    
                Ty = -self.K1*self.yd - self.K2*self.thetad_x - self.K3*self.y - self.K4*self.theta_x
                    
                print(str(Tx*133.454106))
                
                self.mot_x.set_duty(133.454106*Tx)
                self.mot_y.set_duty(133.454106*Ty)
                
                self.transitionTo(self.S1_Values)
                
            else:
                pass
    
            self.next_time = utime.ticks_add(self.next_time, int(self.period*1e6))

    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState      
        
        
if __name__ == "__main__":
    
    import pyb
    from MotorDriver import MotorDriver
    from Encoder import Encoder
    from fault import nFAULT
    from touchDriver import touchDriver
    
    period = 1 #s
    
    # Setting up motors using MotorDriver class
    IN1 = pyb.Pin.cpu.B4
    IN2 = pyb.Pin.cpu.B5
    IN3 = pyb.Pin.cpu.B0
    IN4 = pyb.Pin.cpu.B1
    
    nSLEEP = pyb.Pin.cpu.A15
    nFAULT_pin = pyb.Pin.cpu.B2
    
    CH1 = 1
    CH2 = 2
    CH3 = 3
    CH4 = 4
    
    timer_3 = pyb.Timer(3, freq=20000)
    Mot1_y = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
    Mot2_x = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)
    
    # Setting up enconders using Encoder class
    E1_CH1 = pyb.Pin.cpu.B6
    E1_CH2 = pyb.Pin.cpu.B7
    E2_CH1 = pyb.Pin.cpu.C6
    E2_CH2 = pyb.Pin.cpu.C6
    
    tim4 = pyb.Timer(4)
    tim8 = pyb.Timer(8)

    Enc1 = Encoder(period, tim4, E1_CH1, E1_CH2)
    Enc2 = Encoder(period, tim8, E2_CH1, E2_CH2)
    
    ym = pyb.Pin.cpu.A0
    xm = pyb.Pin.cpu.A1
    yp = pyb.Pin.cpu.A6
    xp = pyb.Pin.cpu.A7
    w  = 99.36
    l  = 176.64
    y0 = w/2
    x0 = l/2
    
    touch = touchDriver(ym, xm, yp, xp, w, l, x0, y0)
    
    K = (-1.3813, -0.16001, -2.2366, -2.2255)
    
    # Setting up fault tripping using the nFAULT class
    fault = nFAULT(nSLEEP)
    
    controller = platformController(period, Mot2_x, Mot1_y, Enc2, Enc1, touch, K)
    
    while True:
        try:
            controller.run()
        except KeyboardInterrupt:
            nSLEEP.low()
            break
            
    
    
    
    
    
            