# -*- coding: utf-8 -*-
"""
@file lab2_main.py

@brief  Testing average reaction time to blinking light by pressing on-board button.

@details When run on a Nucleo STM32, this script uses the on-board timer to calculate the reaction time for the user to press a button when an LED flashes. Although using the Timer object is not
        the only way to achieve this, it was used here in order to practice setting the device up for later labs where it will be required to use timers. 

----------------------------------------------
Parameters:

@param timer Timer object that counts up from 0 to 0x7FFFFFFF
@param blinko LED pin set to output
@param response_list List of all reaction times by user
@param react_time Average value of reaction times captured during run

----------------------------------------------


@author Ethan Miller
@copyright
@date February 19, 2021

"""

import utime   #import utime to use .sleep function for turning on and off LED
import pyb     #import pyb to interact with Nucleo board pins 
import random  #import random to use for finding random time between 2 and 3 seconds

#-----------Variables---------------------------------------------------------------------
timer = pyb.Timer(5)  #create a Timer object (timer 2) to count upwards from 0
timer.init(prescaler = 80, period = 0x7FFFFFFF, mode = timer.UP)
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
off_time = 0
response_list = []
global looking_for_button

#--------------Creating Interrupt Service Routine (ISR)------------------------------------------------

def button_interrupt(which_timer):
    """
    @brief External interrupt called when User button is pressed
    
    @details This external interrupt will save the time of when the button was pressed if the program is looking for a press
    
    @param off_time Saved value of timer when button was pressed.
    
    @author Ethan Miller
    @copyright
    @date February 19, 2021
    
    """
    global off_time                 # make off_time a global variable so it can be used in main code
    if looking_for_button:          # testing to see if the light is on and a button press is valid at this time
        off_time = timer.counter()  # save the timer value to off_time when button was pressed

#-------------------Defining External Interrupt------------------------------------------------------------    
   
extint = pyb.ExtInt(pyb.Pin.board.PC13,     #Setting pin PC13 which is attached to User button 1 (blue button on Nucleo) to activate an EXTERNAL INTERRUPT
                    pyb.ExtInt.IRQ_FALLING, #Interrupt occurs on falling edge of signal
                    pyb.Pin.PULL_NONE,      #No pull up or down resistors are used
                    button_interrupt)       #Define what interrupt service routine to go to when activated (button_interrupt)
    


# -------------------------- MAIN CODE --------------------------------------------

utime.sleep(1)   #sleep for 1 second on boot up, allows user to shift focus to board

while True:      # main code loop
    try:
        utime.sleep(random.randint(2,3)) # wait a random time to turn LED on
        blinko.high()
        
        timer.counter(0) 
        # reset timer to 0 to avoid having to add or subtract timer values. 
        # This is okay in this code because it is the only thing using the timer. 
        # This would NOT be okay if multiple things were based off this one timer.

        looking_for_button = True   #now that LED is on, button press is valid
        utime.sleep(1)              # sleep for a second while button is waiting to be pressed
        blinko.low()
        
        if off_time != 0:                            # if the button was pressed and a valid time was recorded, then...
            response_list.append(off_time/(10**6))   # add the response time (timer time divided by 10^6 because its in microseconds) to the end of list
            off_time = 0           
        else:
            pass
       
        looking_for_button = False              # button press is not valid anymore
        
#--------------------------------------------------------------------------------------------   
     
    except KeyboardInterrupt:                   # if a Keyboard Interupt (CTRL-C) occurs, run this code
        if len(response_list) != 0:
            react_time = sum(response_list)/len(response_list)          # finding average response time
            print("\nYour average reaction time was {:.3f} seconds \n".format(react_time))
        else:
            print("\nNo valid attempts were recorded \n")               #if no response times were recorded
        break

    
    
    
    