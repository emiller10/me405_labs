## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  
#  @section sec_intro Introduction
#  This is the documentation website for Ethan Miller for ME 405 Mechatronics. This portfolio will go through the process of we took during this
#  ME 405 starting with implementing processes to put code onto the microcontroller to coding a balancing panel fit with a PD controller, two motors,
#  two encoders, and a touch panel.
#  \n \image html coding_is_fun.png
#
#  @subsection sec_labs Lab Pages:
#  \ref lab00
#  \n \ref lab01
#  \n \ref lab02
#  \n \ref lab03
#  \n \ref lab04
#  \n \ref lab05
#  \n \ref lab06
#  \n \ref lab07
#  \n \ref lab08
#  \n \ref lab09
#  \n \n \n \n \n \n
#  @author Ethan Miller
#  @date February 16, 2021
#  @copyright License Info

#------------------------------------------------

##
#  @page lab00 Lab 0 
#  In this lab, Doxygen was set up to create the documentation for the
#
#  @subsection sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
#  @subsection sec_enc Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Ethan Miller
#  @date February 16, 2021
#  @copyright License Info

#------------------------------------------------

##  @page lab01 Lab 1 - VendoTRON
#   This lab consisted of creating a virtual vending machine that takes user inputs of monetary values (1 cent to $20),
#   and responds with prompts and giving drinks of the users choosing. 
#   
#  @subsection subsec_doc Documentation
#  Please see \ref lab1_main.py for documentation
#
#  @subsection subsec_source Source Code
#  You can see the source code at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/lab1_main.py
#  @subsection sub_fsm Finite State Machine
#   The Vendotron functions based off the finite state machine shown in Figure 1 and utilizes cooperative multitasking.
#   \n \image html lab1_fsm.png 
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @date February 16, 2021
#  @copyright License Info

#------------------------------------------------

##  @page lab02 Lab 2 - Think Fast!
#   This lab consists of running a script on the Nucleo that will record the user's response time to a LED turning on.
#   
#  @subsection subsec_doc Documentation
#  Please see \ref lab2_main.py for documentation
#
#  @subsection subsec_source Source Code
#  You can see the source code at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/lab2_main.py
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @date February 19, 2021
#  @copyright License Info

#------------------------------------------------

##  @page lab03 Lab 3 - Pushing the Right Buttons
#   This lab consists of a script running on the PC that communicates to the Nucleo over UART to capture voltage data from pressing the blue User button.
#   \n The step response of the button came out to be:
#  \image html step_response_lab3.png
#  @subsection subsec_doc Documentation
#  Please see \ref main_lab3.py for documentation for main file on the Nucleo
#  See \ref UI_lab3.py for documentation for the UI front running on the PC
#
#  @subsection subsec_source Source Code
#  You can see the source code for the main file at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/main_lab3.py <br>
#  and source code for the UI front at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/UI_lab3.py
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab04 Lab 4 - Hot or Not?
#   This lab consists of a script running on the PC that communicates to the Nucleo over UART to capture voltage data from pressing the blue User button.
#   
#  @subsection subsec_doc Documentation
#  Please see \ref main_lab3.py for documentation for main file on the Nucleo
#  See \ref UI_lab3.py for documentation for the UI front running on the PC
#
#  @subsection subsec_source Source Code
#  You can see the source code for the main file at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/main_lab3.py <br>
#  and source code for the UI front at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/UI_lab3.py
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab05 Lab 5 - Feeling Tipsy?
#   This lab consisted of calculating the equations of motion for a two dimensional platform that is balancing a ball on top. FBDs and KDs were created to find these
#   equations. The images below outline the process. Although they are not correct, the calculations did allow me to grasp the concept of the system better when coding
#   and simulating it in later labs.
#   
#   \image html calc_p1.png  <br>
#   \image html calc_p2.png  <br>
#   \image html calc_p3.png  <br>
#   \image html calc_p4.png  <br>
#
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab06 Lab 6 - Simulation or Reality?
#   This lab consisted of simulating the closed and open loop responses of the system analyzed in Lab 5 for the Term Project. In order to simulate, MATLAB was utilized
#   because it is easier to setup state space systems for simulation. This will allows to see how the system will react given different initial conditions over time. 
#   Five different cases were looked at. Descriptions of each will proceed the calculations below.<br>
#   \image html sim_p1.png  <br>
#   \image html sim_p2.png  <br>
#   \image html sim_p3.png  <br>
#   \image html sim_p4.png  <br>
#   \image html sim_p5.png  <br>
#   \image html sim_p6.png  <br>
#   \image html sim_p7.png  <br>
#   
#  @subsection subsec_doc Documentation
#  Please see \ref Lab6_MATLAB.mlx for documentation for MATLAB Script
#
#  @subsection subsec_source Source Code
#  You can see the source code for the main file at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/main_lab3.py <br>
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab07 Lab 7 - Feeling Touchy
#   This lab configures the touch pad that will be used in the term project in order to find the position of the ball on the touch screen. 
#   A  common  way  to  read  from  the  resistive  touch  panel  is  to  scan  the  X,  Y,  and  Z  componentsone at a time.  
#   Here the X component refers to the horizontal location of the contact point,  the Y component refers to the vertical location of the contact point,
#   and the Z component refers towhether or not there is contact with the screen.
#  @subsection subsec_doc Documentation
#  Please see \ref Location.py for documentation for the Location class.
#
#  @subsection subsec_source Source Code
#  You can see the source code for the Location class at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/Location.py <br>
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab08 Lab 8 - Term Project Part 1
#   This lab consisted of creating generic MotorDriver and Encoder classes that can be created to send duty cycles to the motor and 
#   find the angle that the encoder is at.
#   \n \n
#   A motor driver class encapsulates all of the features associated with driving a DC motor connected to the DRV8847 motor 
#   driver that is part of the base unit that we’ve connected the Nucleo to.  The base unit carries the DRV8847 motor driver 
#  from Texas Instruments which can be used to drive two DC motors or one stepper motor.
#  \n \n 
#  A common way to measure the movement of a motor is with an optical encoder.  Because mostencoders can send out signals at high 
#  frequency (sometimes up to 1MHz or so),  we can’t readone with regular code; we need something faster, such as interrupts or 
#  dedicated hardware.  OurSTM32 processors have such hardware – timers which can read pulses from encoders and countdistance and 
#  direction of motion.  Incremental encoders send digital signals over two wires, usu-ally in quadrature, as shown below; 
#  the two signals are about 90 degrees out of phase.
#  \n \image html encoder_logic.png <br>
#  @subsection subsec_doc Documentation
#  Please see \ref MotorDriver.py for documentation on the Motor Driver class. <br>
#  See \ref Encoder.py for documentation on the Encoder class.
#
#  @subsection subsec_source Source Code
#  You can see the source code for the MotorDriver class at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/MotorDriver.py <br>
#  and source code for the Encoder class at:
#  https://bitbucket.org/emiller10/me405_labs/src/master/encoder.py <br>
#   
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info

#------------------------------------------------

##  @page lab09 Lab 9 - Term Project Part 2
#  This Lab uses a motor controller and previously created classes to balance a ball on a touch screen platform.<br>
#  Project video is located at https://cpslo-my.sharepoint.com/:v:/g/personal/emille37_calpoly_edu/EfI-MtpDonVJs4OA5whGoMMBfYKnbdOMBLeWBC6vLpVEWQ?e=vBjnbN <br>
#  Source code is located at https://bitbucket.org/candrews09/me405_lab/src/master/FinalProject/ <br>
#  Partner's main page:  https://candrews09.bitbucket.io
#  Main Controller File used to initialize all pins, objects, and balance the platform: main.py <br>
#  Motor Driver used to control both motors: MotorDriver.py <br>
#  Encoder Driver used to measure the position of the platform: Encoder.py <br>
#  Touch Screen Driver used to measure the position and speed of the ball: touchDriver.py <br>
#  Fault detection used for overvcurrent protection: fault.py <br>
#  \n \n \n \n
#  @author Ethan Miller
#  @copyright License Info