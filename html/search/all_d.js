var searchData=
[
  ['s0_5finit_47',['S0_INIT',['../classmain_1_1platformController.html#a9f04ebdf15c14029b4885f1e540527c7',1,'main::platformController']]],
  ['s1_5fvalues_48',['S1_Values',['../classmain_1_1platformController.html#a094a7ae08d239840c32099379ca87d9a',1,'main::platformController']]],
  ['s2_5fmotors_49',['S2_Motors',['../classmain_1_1platformController.html#a33efdd6c792717596cee84dcef7c7bae',1,'main::platformController']]],
  ['set_5fduty_50',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fduty_5fcycle_51',['set_duty_cycle',['../classmotor_1_1Motor.html#aab6d345560661d43ccdb60a94165cc35',1,'motor::Motor']]],
  ['set_5fposition_52',['set_position',['../classEncoder_1_1Encoder.html#a746743aaf406c52e5a2eb034cf502fcb',1,'Encoder::Encoder']]],
  ['state_53',['state',['../classEncoder_1_1Encoder.html#ab682cdb03d2a5a4a6da43d3ed64c348c',1,'Encoder.Encoder.state()'],['../classmain_1_1platformController.html#a6c304e2e140b2e933ce81c9f78f7c5f8',1,'main.platformController.state()']]]
];
