var lab2__main_8py =
[
    [ "button_interrupt", "lab2__main_8py.html#a7e3bc057ae570ea3912b84eb0605592e", null ],
    [ "blinko", "lab2__main_8py.html#abf73e944af486bf189fdb7be50e43fdb", null ],
    [ "extint", "lab2__main_8py.html#aedaca1dd8708535b47d494c34eb25683", null ],
    [ "looking_for_button", "lab2__main_8py.html#ae0ac9b453d6ba05e669aa9a1581af770", null ],
    [ "mode", "lab2__main_8py.html#a7170c3a719f36a0b8d6811d9395a7fdc", null ],
    [ "off_time", "lab2__main_8py.html#a8207adcc226f767c6546dfb3595c8cbb", null ],
    [ "period", "lab2__main_8py.html#a9ad50963439af4495bd44fb58c9245d0", null ],
    [ "prescaler", "lab2__main_8py.html#a61c143321cf23805c66171163183414e", null ],
    [ "react_time", "lab2__main_8py.html#a0a84e9a0e265b5db6ce7e97f7ab06685", null ],
    [ "response_list", "lab2__main_8py.html#a64b1951a5c339e386ca6e3e47b385fad", null ],
    [ "timer", "lab2__main_8py.html#a679951b3b532140efb22743dc0def8fe", null ],
    [ "x7FFFFFFF", "lab2__main_8py.html#ad176ab22d282a9ea9bc491e5b8b75a32", null ]
];