var annotated_dup =
[
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "fault", null, [
      [ "nFAULT", "classfault_1_1nFAULT.html", "classfault_1_1nFAULT" ]
    ] ],
    [ "Location", null, [
      [ "Location", "classLocation_1_1Location.html", "classLocation_1_1Location" ]
    ] ],
    [ "main", null, [
      [ "platformController", "classmain_1_1platformController.html", "classmain_1_1platformController" ]
    ] ],
    [ "motor", "namespacemotor.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "touchDriver", null, [
      [ "touchDriver", "classtouchDriver_1_1touchDriver.html", "classtouchDriver_1_1touchDriver" ]
    ] ]
];